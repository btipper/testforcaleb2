package com.transform.fizzbuzz;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class FizzBuzzGenerator {

    private static final String FIZZ = "fizz";
    private static final String BUZZ = "buzz";
    private static final String FIZZBUZZ = "fizzbuzz";
    private static final String LUCK = "luck";
    private static final String NUMBER = "number";

    public String generateFrom(int from, int to) {
        String fizzBuzzString = IntStream.rangeClosed(from, to)
                .mapToObj(item -> mapFrom(item))
                .collect(Collectors.joining(" "));

        Map<String, Long> typeCountMap = groupByType(fizzBuzzString);

        String statistics =
                FIZZ + ": " + typeCountMap.get(FIZZ) + "\n" +
                        BUZZ + ": " + typeCountMap.get(BUZZ) + "\n" +
                        FIZZBUZZ + ": " + typeCountMap.get(FIZZBUZZ) + "\n" +
                        LUCK + ": " + typeCountMap.get(LUCK) + "\n" +
                        NUMBER + ": " + typeCountMap.get(NUMBER);

        return fizzBuzzString + "\n" + statistics;
    }

    private Map<String, Long> groupByType(String fizzBuzzString) {
        return Arrays.stream(fizzBuzzString.split(" "))
                .map(item -> mapToGroup(item))
                .collect(groupingBy(Function.identity(), counting()));
    }

    private String mapToGroup(String item) {
        String group;
        switch (item) {
            case FIZZ:
            case BUZZ:
            case FIZZBUZZ:
            case LUCK:
                group = item;
                break;
            default:
                group = NUMBER;
        }

        return group;
    }

    private String mapFrom(int i) {
        if (String.valueOf(i).contains("3")) return LUCK;
        if (i % 15 == 0) return FIZZBUZZ;
        if (i % 3 == 0) return FIZZ;
        if (i % 5 == 0) return BUZZ;

        return String.valueOf(i);
    }
}
